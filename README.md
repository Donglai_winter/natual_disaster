# README #

This is the description of the data downloaded from Prof. Ilan Noy's google site and EM-DAT website .

# DATA SOURCE #

* Prof. Ilan Noy's google site: https://sites.google.com/site/noyeconomics/research/natural-disasters/index
* EMDAT website: http://www.emdat.be/. 


# DATA LOCATION #

* folder: \Dropbox\InProgress\Data\disaster
* EMDAT: Data_natural disaster_201117
* Ilan's data: 
1. Dynamic data (original from the website) ---> Lifeyears_disaster index_Ilan
2. Extracted data source from file 1 ---> extractedl_disaster index_Ilan

# TBC #
- [ ] Get disaster distribution based on it's top 10% severe outcome. 


